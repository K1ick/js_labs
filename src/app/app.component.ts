import { Component } from '@angular/core';
import { Sensor} from './shared/modules/sensor.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  sensors: Sensor[] = [];
  public i: number;
  constructor(){
    for(this.i=0; this.i<10; this.i++){
      this.sensors.push(new Sensor(this.i, "Датчик_"+this.i));
    }
    console.log(this.sensors)
  }
  onDeleteSensor(index:number){
    this.sensors.splice(index,1);
  }
  addSensor(name, status){
    this.i = this.i+1;
    this.sensors.push(new Sensor(this.i, name.value, status.checked))
  }

}
